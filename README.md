# Setting up Cypress with Next

First install the packages:

```bash
yarn install
```

## Cypress setup

* Create an account https://dashboard.cypress.io/signup
* Open Cypress `yarn run cypress open`
* Create a project https://docs.cypress.io/guides/dashboard/projects.html#Setup
* Make sure you have a `cypress.json` file in the root of the project with your project ID
```json
{
  "projectId": "xxxxx"
}
```
* Copy and rename the `.env.sample` file to `.env`
* Copy and paste the Cypress record key as a value for `$CYPRESS_RECORD_KEY`

## Cypress test
We want to run tests on the homepage. For that we need to set the `baseUrl` of our Next App.

Add the `baseUrl` to your `cypress.json`
```json
{
  "projectId": "xxxxx",
  "baseUrl": "http://localhost:3000"
}
```

We'll need this to visit the pages in our app. 

I created a simple example test at `cypress/integration/homepage.spec.js`.

### Running tests in the Cypress tool

1. Make sure your app is running `yarn dev`
2. Open Cypress `yarn run cypress open`
3. Go to the 'Tests' tab
4. Click on `homepage.spec.js`
5. The test will now open in a new browser window and run the test

### Running tests in the shell

To do this I created a npm script which will do the following:

* Build the Next App
* Run the Next App
* Run the Cypress tests
* Record the tests in the online dashboard

To run it:

1. Make sure your port (3000 in this case) is not blocked by your or other apps
2. Run the npm script `yarn test:e2e`
3. In your shell you should see the results
4. Now go to your dashboard: https://dashboard.cypress.io/ and see your test runs

### Running tests in Gitlab CI pipeline 

1. Go to your repo in Gitlab
2. Add the CI variable: `CYPRESS_RECORD_KEY` with your record key (https://docs.gitlab.com/ee/ci/variables/#create-a-custom-variable-in-the-ui)
3. Make sure the variable is *not* protected
4. Check `.gitlab-ci.yml` file to see the configuration of the pipeline
5. Push a branch
6. Check your pipeline in Gitlab
7. See your test run in Cypress Dashboard



