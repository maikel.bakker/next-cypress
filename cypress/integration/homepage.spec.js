/// <reference types="cypress" />

const SELECTORS = {
  PAGE_TITLE: '[data-name=page-title]'
}

context('Homepage', () => {
  before(() => {
    cy.visit('/')
  })

  it('Page title contains the right content', () => {
    cy.get(SELECTORS.PAGE_TITLE)
      .contains('Welcome to')
  })
})
